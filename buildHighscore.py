#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import dominate
import time
from dominate.tags import *

# open database
con = mdb.connect('localhost', 'blodyx', 'V!ik2ing3', 'unikaturfare')
c = con.cursor()
d = con.cursor()
c.execute("SELECT taker, COUNT(*) FROM taken GROUP BY taker ORDER BY COUNT(*) DESC LIMIT 50;")


doc = dominate.document(title='Top50 UnikaTurfare')

with doc.head:
    link(rel='stylesheet', href='/var/www/html/assets/css/style.css')

with doc:
    #    with div(id='header').add(ol()):
    #        for i in ['home', 'about', 'contact']:
    #            li(a(i.title(), href='/%s.html' % i))

    with div(ALIGN='center'):
        #           attr(cls='body')
        h1('Top50 UnikaTurfare!')
        rows = c.fetchall()
        with table(border='0').add(tbody()):
            for row in rows:
                l = tr()
                d.execute("SELECT * FROM users WHERE id = %s", (row[0],))
                user = d.fetchone()
        #   print row
                try:
                    l += td(user[1], ALIGN='right')
                except Exception, e:
                    l += td('NULL', ALIGN='right')
     #          print repr(e)
                l += td(row[1])
        p(time.ctime())
print doc


# Save (commit) the changes and close
con.commit()
con.close()

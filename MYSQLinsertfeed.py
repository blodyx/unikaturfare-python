# !/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb
import json
# import datetime
from urllib2 import urlopen
# import dateutil.parser
import urllib as ul

# open database
con = MySQLdb.connect('localhost', 'root', 'VarandetNu13', 'unikaturfare')
c = con.cursor()

# get timestamp
c.execute("SELECT tid FROM timestamp;")
timestamp = c.fetchone()
timestamp = str(ul.quote(timestamp[0]))
url = 'http://api.turfgame.com/unstable/feeds/takeover?afterDate='+timestamp

# read API
res = urlopen(url)
data = json.load(res)

entrys = (len(data))
for x in range(0, entrys):
    try:
        taker = data[x]['currentOwner']['id']
        giver = data[x]['zone']['previousOwner']['id']
        c.execute("INSERT INTO taken VALUES (%s,%s)", (taker, giver))
    except Exception as e:
        print(e)

try:
    time = data[0]['time']
    c.execute("REPLACE INTO timestamp VALUES (%s, %s)", (0, time))
except Exception as e:
    pass

# Save (commit) the changes and close
con.commit()
con.close()
